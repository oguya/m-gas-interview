from flask import Flask, request, jsonify, abort, make_response
from os import getenv

app = Flask(__name__)
app.config["DEBUG"] = True


library = [
    {
        'id': 1,
        'title': 'Systems Performance',
        'author': 'Brendan Gregg',
        'year': '2013'
    },
    {
        'id': 2,
        'title': 'Permanent Record',
        'author': 'Edward Snowden',
        'year': '2019'
    },
    {
        'id': 3,
        'title': 'The Challenge for Africa',
        'author': 'Wangari Maathai',
        'year': '2009'
    }
]


@app.route('/', methods=['GET'])
def index():
    return '''
<h1>Home Library</h1>
<p>A simple Flask API for my personal library.</p>
<h3>Usage</h3>
<li>List all books: <a href="/api/v1/books/all">/api/v1/books/all</a></li>
<li>Details about a specific Book(ID 2): <a href="/api/v1/books/2">/api/v1/books/:id</a></li>
'''


# returns a list of the all the books in the lib.
@app.route('/api/v1/books/all', methods=['GET'])
def books():
    return jsonify(library)


# returns details of a specific book given it's ID
# sample url: /api/v1/books/book_id
@app.route('/api/v1/books/<int:book_id>', methods=['GET'])
def book(book_id):
    book = [book for book in library if book['id'] == book_id]
    if len(book) == 0:
        abort(400)
    return jsonify(book[0])


@app.errorhandler(400)
def error_handling(error):
    return make_response(jsonify({'error': 'Bad request'}), 400)

if __name__ == '__main__':
    app.run()
