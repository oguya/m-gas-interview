FROM ubuntu:20.04

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
    bash=5.0-6ubuntu1.1 \
    curl=7.68.0-1ubuntu2.5 \
    gcc=4:9.3.0-1ubuntu2 \
    git=1:2.25.1-1ubuntu3.1 \
    jq=1.6-1ubuntu0.20.04.1 \
    openssh-client=1:8.2p1-4ubuntu0.2 \
    python3-dev=3.8.2-0ubuntu2 \
    python3=3.8.2-0ubuntu2 \
    python3-pip=20.0.2-5ubuntu1.4 \
    unzip=6.0-25ubuntu1 \
    openssl=1.1.1f-1ubuntu2.3 \
    && rm -rf /var/lib/apt/lists/*

RUN pip3 install --upgrade pip==21.0.1 && \
    pip3 install \
    awscli==1.19.26 \
    boto3==1.17.4 \
    jinja2==2.10.3 \
    cfn-lint==0.49.1
