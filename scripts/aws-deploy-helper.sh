#!/usr/bin/env bash

STACK_NAME="$1"
APP_ENV="$2"
WebAppImage="$3"

ECS_CLUSTER_NAME="ecs-cluster-dev"
ECS_SERVICE_TPL="iac/cloudformation/app-service.yaml"

echo "Retrieving ListenerARN & ListenerRulePriority..."

listener_arn=$(
aws cloudformation describe-stacks \
--stack-name "$ECS_CLUSTER_NAME" \
--query 'Stacks[0].Outputs[?OutputKey==`ListenerARN`].OutputValue' \
--output text
)

listener_priority=$(
aws cloudformation describe-stacks \
  --stack-name "$STACK_NAME" \
  --query 'Stacks[0].Parameters[?ParameterKey==`ListenerRulePriority`].ParameterValue' \
  --output text
)

if [[ -z $listener_priority ]]; then
    listener_priority=$(echo $(( 1 + $RANDOM % 49999 )))
fi

echo "Deploying stack $STACK_NAME on $APP_ENV env ..."

aws cloudformation deploy \
  --stack-name "$STACK_NAME" \
  --template-file $ECS_SERVICE_TPL \
  --capabilities CAPABILITY_NAMED_IAM \
  --parameter-overrides \
    EnvironmentTag="$APP_ENV" \
    ECSClusterName="$ECS_CLUSTER_NAME" \
    ECSServiceRole="${ECS_CLUSTER_NAME}-ServiceRole-eu-west-1" \
    WebAppImage="$3" \
    ALBListenerARN="$listener_arn" \
    ListenerRulePriority=$listener_priority \
  --tags \
    Name="$STACK_NAME" \
    Environment="$APP_ENV"
